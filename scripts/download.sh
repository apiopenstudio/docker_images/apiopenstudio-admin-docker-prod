#!/usr/bin/env bash

CI_REPOSITORY=$1
BRANCH=$2
TAG=$3
FILE_EXTENSION="zip"

REPO_NAME=${CI_REPOSITORY##*/}

if [ -n "$(echo "${CI_REPOSITORY}" | grep "github.com")" ] ; then
  ARCHIVE_PATTERN="BRANCH_TAG"
  URL_PATTERN="CI_REPOSITORY/archive/refs/REFS/ARCHIVE_NAME.FILE_EXTENSION"
elif [ -n "$(echo "${CI_REPOSITORY}" | grep "gitlab.com")" ] ; then
  ARCHIVE_PATTERN="REPO_NAME-BRANCH_TAG"
  URL_PATTERN="CI_REPOSITORY/-/archive/BRANCH_TAG/ARCHIVE_NAME.FILE_EXTENSION"
else
  echo "Could not match repository domain."
  exit 1
fi

if [ ! -z "${BRANCH}" ] ; then
  REFS="heads"
  BRANCH_TAG="${BRANCH}"
elif [ ! -z "${TAG}" ] ; then
  REFS="tags"
  BRANCH_TAG="${TAG}"
else
  echo "Could not match branch or tag."
  exit 1
fi

ARCHIVE_NAME=$(echo "${ARCHIVE_PATTERN}" | \
  sed "s|REPO_NAME|${REPO_NAME}|g" | \
  sed "s|BRANCH_TAG|${BRANCH_TAG}|g" | \
  sed "s|\/|-|g")

URL=$(echo "${URL_PATTERN}" | \
  sed "s|CI_REPOSITORY|${CI_REPOSITORY}|g" | \
  sed "s|REFS|${REFS}|g" | \
  sed "s|ARCHIVE_NAME|${ARCHIVE_NAME}|g" | \
  sed "s|FILE_EXTENSION|${FILE_EXTENSION}|g" | \
  sed "s|BRANCH_TAG|${BRANCH_TAG}|g")

curl -L "${URL}" -o "${ARCHIVE_NAME}.${FILE_EXTENSION}"
unzip "${ARCHIVE_NAME}.${FILE_EXTENSION}"
rm -R /var/www/html
mv "${ARCHIVE_NAME}" /var/www/html
rm "${ARCHIVE_NAME}.${FILE_EXTENSION}"
