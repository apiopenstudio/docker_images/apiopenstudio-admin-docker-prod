server {
    listen 80 default_server;
    listen [::]:80 default_server;

    server_name _;

    return 301 https://$host$request_uri;
}

server {
    listen 443 ssl default_server;
    listen [::]:443 ssl default_server;

    server_name _;

    ssl_certificate /etc/nginx/certs/apiopenstudio_admin.crt;
    ssl_certificate_key /etc/nginx/certs/apiopenstudio_admin.key;

    index index.php;
    root /var/www/html/public;

    location / {
        try_files $uri /index.php$is_args$args;
    }

    location ~ \.php$ {
        try_files               $uri =404;
        fastcgi_split_path_info ^(.+\.php)(/.+)$;
        include                 fastcgi_params;
        fastcgi_param           SCRIPT_FILENAME $realpath_root$fastcgi_script_name;
        fastcgi_param           SCRIPT_NAME $fastcgi_script_name;
        fastcgi_index           index.php;
        fastcgi_pass unix:/run/php/php8.1-fpm.sock;
    }

    location ~* \.(js|jpg|png|svg|css)$ {
        expires 1d;
    }

    location ~ /\.ht {
        deny all;
    }
}
